# Electron Messenger
Facebook Messenger as a desktop app using Electron. This project wrap https://www.messenger.com to a desktop app using [Electron](http://electron.atom.io/) to render the page in a native app.

### Run
Download and unpack [Electron](http://electron.atom.io/).
Run command

    electron <path to folder with files from this project>

### Develop
Run the following commands:

    npm install
    npm start




