var {app, BrowserWindow, Tray, Menu, MenuItem} = require('electron');
var fs = require('fs');


// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the javascript object is GCed.
var mainWindow = null;
let tray = null;
let trayMenu = null;
const mainMenu = new Menu();

let hideToTray = true;
const showOnStart = true;
const messengerUrl = 'https://www.messenger.com';




// Quit when all windows are closed.
app.on('window-all-closed', function() {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

// This method will be called when Electron has done everything
// initialization and ready for creating browser windows.
app.on('ready', function() {
    // Create the browser window.
	mainWindow = new BrowserWindow({
		width: 600,
		height: 500,
		show: showOnStart,
		useContentSize: true,
		webPreferences: {
			nodeIntegration: false
		},
		title: 'Messenger',
		icon: __dirname + '/icon.png'
	});
	
	// tray
	tray = new Tray(__dirname + '/icon.png')
    trayMenu = new Menu();
    const quitItem = new MenuItem({label: 'Quit', type: 'normal', click: quit});
    const showItem = new MenuItem({id: 'show', label: 'Show', type: 'normal', click: showWindow, visible: !showOnStart});
    const hideItem = new MenuItem({id: 'hide', label: 'Hide', type: 'normal', click: hideWindow, visible: showOnStart});
    trayMenu.append(quitItem);
    trayMenu.append(showItem);
    trayMenu.append(hideItem);
    tray.setToolTip('Facebook messenger');
    tray.setContextMenu(trayMenu);
    
    // main menu
    const fileMenu = new Menu();
	fileMenu.append(quitItem);
	const fileItem = new MenuItem({type: 'submenu', label: 'File', submenu: fileMenu});
	
	const viewMenu = new Menu();
	viewMenu.append(new MenuItem({label: 'Reload', click: load}));
	const viewItem = new MenuItem({type: 'submenu', label: 'View', submenu: viewMenu});

	mainMenu.append(fileItem);
	mainMenu.append(viewItem);
	// mainMenu.append(new MenuItem({label: 'Dev-tools', role: 'toggleDevTools'}));
	Menu.setApplicationMenu(mainMenu);

	load();

	mainWindow.on('close', function(event) {
		if (hideToTray) {
			event.preventDefault();
			hideWindow();
		}
	});

    mainWindow.on('closed', function() {
		trayMenu = null;
		tray = null;
		mainWindow = null;
    });
});

function showWindow() {
	mainWindow.show();
	trayMenu.getMenuItemById('show').visible = false;
	trayMenu.getMenuItemById('hide').visible = true;
	tray.setContextMenu(trayMenu);
};

function hideWindow() {
	mainWindow.hide();
	trayMenu.getMenuItemById('hide').visible = false;
	trayMenu.getMenuItemById('show').visible = true;
	tray.setContextMenu(trayMenu);
};

function quit() {
	hideToTray = undefined;
	mainWindow.close();
};

function load() {
	mainWindow.loadURL(messengerUrl);
};
